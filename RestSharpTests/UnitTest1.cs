﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;

namespace RestSharpTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        [Obsolete]
        public string Login()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            RestClient client = new RestClient("https://localhost:50000/");
            RestRequest request = new RestRequest("b1s/v1/Login", DataFormat.Json);

            var data = new
            {
                CompanyDB = "Seva_1003_Dry_Serialized",
                UserName = "manager",
                Password = "Seva@123"
            };

            request.AddBody(data);
            return client.Post(request).Content;
        }

        [TestMethod]
        [Obsolete]
        public void GetSalesInvoices()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            RestClient client = new RestClient("https://localhost:50000/");
            RestRequest request = new RestRequest("b1s/v1/Invoices", Method.GET);

            request.AddParameter("B1SESSION", "279ae9d6-eab4-11eb-8000-44850013bfa0", ParameterType.Cookie);

            var response = client.Execute(request);
        }

        [TestMethod]
        [Obsolete]
        public void PostSalesInvoice()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            RestClient client = new RestClient("https://localhost:50000/");
            RestRequest request = new RestRequest("b1s/v1/Invoices", DataFormat.Json);

            request.AddParameter("B1SESSION", "fe274a32-eada-11eb-8000-44850013bfa0", ParameterType.Cookie);

            var data = new
            {
                CardCode = "C900001",
                DocumentLines = new[] {
                    new {
                        ItemCode = "PG-CL-I-2",
                        Quantity = "1",
                        TaxCode = "UT",
                        BatchNumbers = new [] {
                            new {
                                BatchNumber = "PG-CL-I-2-B-1",
                                Quantity = "1"
                            }
                        }
                    },
                }
            };

            request.AddBody(data);
            var response = client.Post(request);
        }

        [TestMethod]
        [Obsolete]
        public void GetRetailSale()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            RestClient client = new RestClient("http://localhost:3000/");
            RestRequest request = new RestRequest("sale", DataFormat.Json);

            var JsonResponse = JObject.Parse(client.Get(request).Content);

            var DocumentLines = JsonResponse["DocumentLines"];
            var CardCode = JsonResponse["CardCode"];
            var DocumentStatus = JsonResponse["DocumentStatus"];
            var ItemCode = DocumentLines[0]["ItemCode"];
            var Quantity = DocumentLines[0]["Quantity"];
            var TaxCode = DocumentLines[0]["TaxCode"];
            var BatchNumber = DocumentLines[0]["BatchNumbers"][0]["BatchNumber"];
            var BatchQuantity = DocumentLines[0]["BatchNumbers"][0]["Quantity"];
        }

        [TestMethod]
        [Obsolete]
        public void RetailToSalesInvoice()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            var SessionId = JObject.Parse(Login())["SessionId"];

            RestClient RetailClient = new RestClient("http://localhost:3000/");
            RestRequest RetailRequest = new RestRequest("sale", DataFormat.Json);

            var JsonResponse = JObject.Parse(RetailClient.Get(RetailRequest).Content);

            var DocumentLines = JsonResponse["DocumentLines"][0];
            var CardCode = ((JValue)JsonResponse["CardCode"]).Value;
            var DocumentStatus = ((JValue)JsonResponse["DocumentStatus"]).Value;
            var ItemCode = ((JValue)DocumentLines["ItemCode"]).Value;
            var Quantity = ((JValue)DocumentLines["Quantity"]).Value;
            var TaxCode = ((JValue)DocumentLines["TaxCode"]).Value;
            var BatchNumber = ((JValue)DocumentLines["BatchNumbers"][0]["BatchNumber"]).Value;
            var BatchQuantity = ((JValue)DocumentLines["BatchNumbers"][0]["Quantity"]).Value;

            var data = new
            {
                CardCode = CardCode,
                DocumentLines = new[] {
                    new {
                        ItemCode = ItemCode,
                        Quantity = Quantity,
                        TaxCode = TaxCode,
                        BatchNumbers = new [] {
                            new {
                                BatchNumber = BatchNumber,
                                Quantity = BatchQuantity
                            }
                        }
                    },
                }
            };

            RestClient B1Client = new RestClient("https://localhost:50000/");
            RestRequest B1Request = new RestRequest("b1s/v1/Invoices", DataFormat.Json);

            B1Request.AddParameter("B1SESSION", SessionId, ParameterType.Cookie);

            B1Request.AddBody(data);
            var response = B1Client.Post(B1Request);
        }

        [TestMethod]
        [Obsolete]
        public void SalesInvoiceSToRetail()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            var SessionId = JObject.Parse(Login())["SessionId"];

            RestClient client = new RestClient("https://localhost:50000/");
            RestRequest request = new RestRequest("b1s/v1/Invoices(1)", Method.GET);

            request.AddParameter("B1SESSION", SessionId, ParameterType.Cookie);

            var response = client.Execute(request);

            RestClient RetailClient = new RestClient("http://localhost:3000/");
            RestRequest RetailRequest = new RestRequest("print_sales", DataFormat.Json);

            RetailRequest.AddBody(response.Content);
            RetailClient.Post(RetailRequest);
        }
    }
}
